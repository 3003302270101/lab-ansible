let chai = require('chai');
let chaiHttp = require('chai-http');
const expect = require('chai').expect;

let HOST = process.env.HOST_SERVICIO1;
let PORT = process.env.PUERTO_SERVICIO1;
let URL = `http://${HOST}:${PORT}/`;

console.log(URL);

chai.use(chaiHttp);


describe('Pruebas unitarias de Servicio1', () => {

	let method_path = 'dividir';

	it('El resultado de la división debe ser correcto.', (done) => {
		
		let valor1 = 400;
		let valor2 = 4;

		let status_esperado = 200;
		let resultado_esperado = 50;

		chai
			.request(URL)
			.post(method_path)
			.set("Content-Type", "application/json")
			.send({
				valor1: valor1,
				valor2: valor2
			})
			.end(function(err, res) {
				if(err) {
					done(err);
				} else {
					expect(res).to.have.status(status_esperado);
					expect(res.body.resultado).to.equal(resultado_esperado);
					done();
				}
			});
	});
});