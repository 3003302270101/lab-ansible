'use strict';

const express = require('express');
const bodyParser = require('body-parser');

// Constants
const PORT = process.env.PUERTO_SERVICIO1;
const HOST = process.env.HOST_SERVICIO1;
const MENSAJE = process.env.MENSAJE;
const IP_BASEDATOS = process.env.IP_BASEDATOS;

// App
const app = express();

app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));

app.get('/', (req, res) => {
  res.send('Hola AyD2 :)');
});
app.get('/test/servicio1', (req, res) => {
  res.send('Mi mensaje es: ' + MENSAJE);
});

app.post('/dividir', (req, res) => {

  console.log(req.body);
  let valor1 = req.body.valor1;
  let valor2 = req.body.valor2;

  let resultado = valor1/valor2;

  res.status(200).send({
    resultado: resultado
  });

});


app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);
